package stackAlgorithm;

import java.util.*;
public class ConvertBase2 {
	
	    // you need treat n as an unsigned value
	    static int reverseBits(int n) {
	        String inputBit = Integer.toString ( n, 2 );
	        System.out.println ( inputBit );
	        Stack<Integer> stack = new Stack<Integer>();
	        
	        int i = 0;
	        for ( i = 0; i < inputBit.length(); i++  ) {
	        	stack.push( Integer.parseInt( inputBit.substring(i, i+1 ) ) ); 
	        }
	        
	        String swapped = "" ;
	        for ( i = 0; i < inputBit.length(); i++  ) {
	        	swapped = swapped + stack.pop().toString() ;
	        }
	        System.out.println( "Swapped = "+swapped );
	        
	        Integer result = Integer.parseInt(swapped.trim() );
	        String res = 		Integer.toString( result , 10 );
	        System.out.println( "Final result = "+res );
	        return 0;
	    }
	
	    
	    public static void main ( String args[]) {
	    	
	    	reverseBits ( 43261596 );
	    }

}

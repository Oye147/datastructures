package stackAlgorithm;

import java.util.Stack;
/* COMPLETED CHecks parenthesis open and close matching
 * return true if open and close
 */

public class CheckParenthesis {
	
	static boolean checkNow ( String str ) {
		
		if (str.isEmpty() ) 
			return true;
		
		Stack<Character> stack = new Stack<Character>();
		
		for (int i = 0; i < str.length(); i++ ) {
			
			char current = str.charAt(i);
			System.out.println( "Current = "+current);
			if ( current == '(' || current == '{' ) 				
				stack.push( current );
			
			if ( current == ')' || current == '}' )
			{
				
				if ( stack.isEmpty()) 
					return false;
			    
			    char last = stack.peek();
			    System.out.println( "last = "+last );
			    if ( current == ')'  &&  last == '(' ||  current == '}'  &&  last == '{' ) {
			    	stack.pop();
			    } else {
			    	return false;
			    }
			}
		}
		return stack.isEmpty();
	}
	
	public static void main ( String[] args ) {
		
		boolean result = checkNow ( "({}89)");
		System.out.println( "Result = "+ result);
		
	}

	
}

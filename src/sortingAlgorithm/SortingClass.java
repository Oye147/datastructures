package sortingAlgorithm;

public class SortingClass {
	
	static void BubbleSort ( int[] arr, int n  ) {
		
		for ( int i = 0; i < n-1; i++ ) {
			for ( int j = 0; j < n - i - 1; j++) {
				if ( arr[j] > arr[j+1] ) {

					int temp = arr[j]; 
					arr[j] = arr[j+1];
					arr[j+1] = temp;
				}
			}
		}
		
		System.out.println("Print Array  -  ");
		 for (int i=0; i<n; ++i) 
	            System.out.print(arr[i] + " "); 
	}
	
	static void InsertionSort ( int [] arr, int n ) {
		System.out.println( "Begin Insertion Sort...  ");
		for ( int i = 1; i < n; i++ ) {
			int key = arr[i];
			int j = i - 1;
			System.out.println("----------- Key = "+key + ", j = "+j);
			
			while ( j >= 0 && arr[j] > key  ) {
				System.out.println ( "arr[j+1] = "+ arr[j+1] + ", arr[j] = " +  arr[j] );
				arr[j+1] = arr[j];
				
				j = j - 1;
			}
			arr[j+1] = key;
		}
		System.out.println("Print Array  -  ");
		for (int i=0; i<n; ++i) 
	            System.out.print(arr[i] + " ");
	}
	
	public static void main ( String args[] ) {
		int arr[] = { 1, 12, 4, 13, 6, 7, 10 }; 
        int n = arr.length; 
        InsertionSort ( arr , n );        
	}

}

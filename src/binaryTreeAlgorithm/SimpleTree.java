package binaryTreeAlgorithm;

public class SimpleTree {
	
	/* 
	 * Simple Tree is a Binary Tree
	 * Maximum number of Nodes at level x = 2 ^x-1
	 * Maximum number of Nodes in a BT of height h = 2 ^ h-1
	 * In a binary
	 * 
	 */

	class Node
	{
		int key;
		Node left, right;
		
		public Node ( int item) {
			key = item;
			left = right = null;
		}
	}
	
	class BinaryTree {
		
		Node root;
		
		BinaryTree(int key) {
			root = new Node(key);	
		}
		
		BinaryTree() {
			root = null;
		}
		
		public void main ( String [] args ) {
			
			BinaryTree tree = new BinaryTree();
			
			tree.root = new Node(1);
			
			tree.root.left = new Node(2);
			tree.root.right = new Node(3);
		}
	}
}

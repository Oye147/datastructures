package binaryTreeAlgorithm;

public class BinarySearchTreeTraversal {
	/* Completed. 
	 *  (1) Depth first Travesal
	 *  (2) Breath first traversal
	 */
	
	static class Node {
		int key;
		Node right, left;
		
		public Node ( int d) {
			key = d;
			left = right = null;
		}
	}
	
    static class BinaryTree {
		
		public Node root;
		
		public BinaryTree() {
			root = null;
		}		
		
		/* ---------------------------------------------------------------------------------
		 * Depth first traversal Begin
		 * 3 Methods Inorder, Preorder, Postorder
		 */
		
	    void Inorder (Node node ) {		    	
			if (node==null)
				return;			
			Inorder( node.left);			
			System.out.print( node.key + " ");			
		    Inorder( node.right );			
		}
	    
	    void Preorder (Node node ) {		    	
			if (node==null)
					return;
				System.out.print( node.key + " ");	
				Inorder( node.left);	
			    Inorder( node.right );			
		}
	    
	    void Postorder (Node node ) {		    	
				if (node==null)
					return;					
				Inorder( node.left);	
			    Inorder( node.right );		
			    System.out.print( node.key + " ");
		}
	
	    void printInorder()  {    Inorder(root);  } 	    
	    void printPreorder()  {    Preorder(root);  } 
	    void printPostorder()  {    Postorder(root);  }  
	    
		/* Depth first traversal End 
		 *  --------------------------------------------------------------------------
		 */
	    
	    
	    
	    /* Breadth First Traversal of Begin
	     * 
	     */	    	    
	    void printLevelOrder() {
	    	int h = height(root);
	    	int i;
	    	for ( i=1; i<=h; i++ ) {
	    		printGivenLevel ( root, i);
	    	}
	    	System.out.println("This is Height = "+h);
	    }
	    
	    void printGivenLevel ( Node root, int level) {
	    	
	    	if (root==null)
	    		return;
	    	
	    	if (level == 1)  {
	    		System.out.println (  root.key + " " );
	    	} else if ( level > 1 ) {
	    		
	    		printGivenLevel ( root.left, level-1 );
	    		//System.out.println ( "Level "+level+ " " + root.key + " " );
	    		printGivenLevel ( root.right, level-1 );
	    	}
	    }
	    
	    int height ( Node node) {
	    	
	    	if (node == null ) {
	    		return 0;
	    	} else {
	    		int lheight = height ( node.left );
	    		int rheight = height ( node.right );
	    		
	    		if ( lheight > rheight ) {
	    			return ( lheight+1);
	    		} else {
	    			return ( rheight+1);	    			
	    		}
	    	}
	    }    
		/* Breadth first traversal End 
		 *  --------------------------------------------------------------------------
		 */

	}
    
    
	public static void main ( String[] args) {		
		BinaryTree tree = new  BinaryTree();
		tree.root = new Node(1);
		tree.root.left = new Node(2);
		tree.root.right = new Node(3);
		tree.root.right.right = new Node(6);
		tree.root.left.left = new Node(4);
		tree.root.left.right = new Node(5);
		tree.root.left.right.right = new Node(7);
		
		/* Depth First 
		tree.printInorder();     // Depth First Inorder
		System.out.println( " ");
		tree.printPreorder();    // Depth First Preorder
		System.out.println( " ");
		tree.printPostorder();   // Depth First Postorder
		*/
		
		/* Breadth First
		*/

		tree.printLevelOrder();  // BreathFirst Traversal
	}

}

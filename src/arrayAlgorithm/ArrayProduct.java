package arrayAlgorithm;

public class ArrayProduct {

	/* Multiply all element of an Array n by space d 
	 * There are 3 methods of swapping arrays 
	 * (1) Using traversing the array  -  Time = O(n), Space = O(1)
	 * */
	
	private static int getProduct ( int [] arr, int n ) {
		
		// Result must be 1. Cannot be 0;
		int result = 1;
		
		for ( int i=0; i < n; i++ ) {
			result = result * arr[i]; 
		}
		return result;
	}
	
	public static void main ( String [] args ) {
		
		int ar[] = { 1, 2, 3, 4, 5 }; 
        int n = ar.length; 
        System.out.println( "Result = "+ getProduct(ar, n)); 
	}
}

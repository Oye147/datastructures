package arrayAlgorithm;

public class ArrRotation {

	/* Rotate an Array n by space d 
	 * There are 3 methods of swapping arrays 
	 * (1) Using a Temp array          -  Time = O(n), Space = O(d)
	 * (2) Using a One by one          -  Time = O(n * d), Space = O(1) 
	 * (3) Using a Juggling Algorithm
	 * 
	 * */
	
	public static void main (String [] args  ) {
		System.out.println ( "Rotating Arrays" );
		int arr[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		int d = 3;
		
		rotateArrayTemp( arr, d );
		
		rotateArrayOneByOne ( arr, d );
	}
	
	/* USING A TEMP ARRAY */
	public static void rotateArrayTemp ( int [] arr, int d) {
		
		int [] tmpArray =  new int[d];
		
		int i, b = 0;
		
		for (int j=0; j < d; j++ ) 
			tmpArray[j] = arr[j];

		for (i=0; i < arr.length-d; i++ ) 
			arr[i] = arr[i+d];
		
		for (int a=i; a < i+d ; a++  ) {
			arr[a] = tmpArray[b];
			b++;
			
		}
		printArray( arr );
	}

	/* USING A One by One */
	public static void rotateArrayOneByOne ( int[] arr, int d ) {
		
		for (int i=0; i < d; i++ ) {
			rotateOnebyOne( arr );
		}
		printArray ( arr );
	}
	
	private static int[] rotateOnebyOne ( int[] arr ) {

		int tmp = arr[0];
		int i;
		
		for (i=0; i < arr.length-1; i++ ) {
			arr[i] = arr[i+1]; 
		}
		arr[i] = tmp;
		return arr;
	}
	
	
	/* USING A juggling Algorithm */
	public void rotateArrayJuggling ( int arr ) {
		
	}
	
	public static void printArray(int[] arr) {
		System.out.println( "Print Array" );
		for (int i=0;i < arr.length;i++ ) {
			System.out.print ( arr[i]);
		}
		System.out.println( "" );
	}

	
}

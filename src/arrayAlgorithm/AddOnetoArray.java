
package arrayAlgorithm;

public class AddOnetoArray {
	/* Completed Add one to Array
	 * if greater than 9, then carry over to next array
	 */

	static void addOne ( int[] arr ) {
		int n = arr.length;
		int [] tmpArray ;
		int i = n - 1;
		int carry = 1;
		while ( i > -1 ) {
			System.out.println( "i = "+i );
			
			if ( arr[i] < 9 && i != 0 ) {
				int sum =  arr[i] + carry;
				arr[i] = sum;
				carry = 0;
			} else if ( arr[i] == 9 && i != 0 ){ 
				arr[i] = 0;
				carry = 1;
			} else if ( arr[i] == 9 && i == 0 ) {
				
				tmpArray = new int[n + 1];
				for (int a = 0; a < tmpArray.length; a++ ) {
					tmpArray[a] = 0;
				}
				tmpArray[0] = 1;
				System.out.println( "TempArray");
				for (int b = 0; b < tmpArray.length; b++ ) {
					System.out.print( " "+tmpArray[b]);
				}
			}
			i--;
		}
		/*
		for (int j = 0; j < arr.length; j++ ) {
			System.out.print( " "+arr[j]);
		}
		*/
	
		
		//System.out.println("result = "+ arr.toString() );
	}
	
	public static void main (String args[] ) {
		int [] arr =  { 9, 9, 9, 9 } ;
		addOne ( arr );
		
		
		
	}
}

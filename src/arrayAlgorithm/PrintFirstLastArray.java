package arrayAlgorithm;

import java.util.Arrays;
/*  COMPLETED : for a given array, print a pair of 
 *  highest and lowest side by side
 */
public class PrintFirstLastArray {
	
	static void SortNow ( int[] arr, int n) {
		
		Arrays.sort( arr );
		
		int i = 0; 
		int j = n-1;
		while ( i < j ) {
			System.out.print( " "+ arr[j] );
			System.out.print( " "+ arr[i] );
			i++;		
			j--;
		}
		
		if ( n % 2 != 0 ) {
			System.out.print( " "+ arr[i] );
		}
		
	}
	
	public static void main ( String args[] ) {
		int arr[] = {1, 12, 4, 6, 7, 10, 13}; 
        int n = arr.length; 
        SortNow ( arr , n );
        
	}

}

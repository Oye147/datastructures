package arrayAlgorithm;

public class ArrayReversal {
	
	/* There are 2 methods of handling this case
	 * (1) In a non recursive way
	 * (2) A recursive way
	 */
	
	static void reverseArray(int [] arr, int start, int end ) {
		/* Non Recursive way */
		int temp;
		
		while ( start < end ) {
			temp =  arr[start];
			arr[start] = arr[end];
			arr[end] = temp;
			start++;
			end--;
		}
		

	}
	
	static void printArray(int arr[],  
             int size) { 
		for (int i = 0; i < size; i++) 
		System.out.print(arr[i] + " "); 
		
		System.out.println(); 
	}  
		
	public static void main( String [] args) {
		
		int [] num = { 1,2,3,4,5,6,7 };
		int n = 7;
		
		printArray ( num, n );
		
		int start = 0;
		int end = n-1;
		
		reverseArray ( num, start, end );
		
		//rvereseArray ( num, start, end );
		
		printArray ( num, n );
	}

	
	
	 static void rvereseArray(int arr[], int start, int end) {
	    /* Recursive way */
	        int temp; 
	        if (start >= end) 
	            return; 
	        temp = arr[start]; 
	        arr[start] = arr[end]; 
	        arr[end] = temp; 
	        rvereseArray(arr, start+1, end-1); 
	        
	    } 
}

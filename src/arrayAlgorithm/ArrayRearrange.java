package arrayAlgorithm;

import java.util.Arrays;

public class ArrayRearrange {
	
	/*
	 * the array such that elements at even positions are greater than all elements 
	 * before it and elements at odd positions are less than all elements before it.
	 */
	
	
	public static void main ( String [] args ) {
		
		int [] arr =  { 1, 2, 3, 4, 5, 6, 7, 8 };
		
		rearrange ( arr , 8 );
		
	}
	
	static void printArray(int arr[],  
            int size) { 
		for (int i = 0; i < size; i++) 
			
		System.out.print(arr[i] + " "); 
		
		System.out.println(); 
	}  
	
	private static void rearrange ( int [] arr, int n ) {
		
		//Arrays.sort( arr );

		//printArray ( arr, n );
		
		int evenPos = n/2;
		System.out.println("evenPos = "+evenPos); 
		int oddPos = n - evenPos;
		System.out.println("oddPos = "+oddPos); 
		
		int [] tempArray = new int[n];
		
		for ( int i = 0; i < n; i ++ )
			tempArray[i] = arr[i];
		
		// sort the auxilliary array
		Arrays.sort( tempArray );
		
		int j = oddPos - 1;
		
		for (int i = 0; i < n; i += 2) { 
            arr[i] = tempArray[j]; 
            j--; 
        } 
		printArray ( arr, n );
		
		j = oddPos; 
		
		for (int i = 1; i < n; i += 2) { 
            arr[i] = tempArray[j]; 
            j++; 
        } 
		 
		printArray ( arr, n );
		
	}

}

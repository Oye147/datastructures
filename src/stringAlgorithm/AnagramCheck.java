package stringAlgorithm;

import java.util.Arrays; 
/* Completed. Anagram of elements in an Array
 * each element checks against every other element
 */

public class AnagramCheck {
	
	static void anCheck ( String[] arr ) {
		int i;
		int n = arr.length;
		for ( i = 0; i < n; i++ ) {
			int len = arr[i].length();
			String sotSet =  sortString ( arr[i] );
			System.out.println(  arr[i] + " - " + sotSet + " = " + len  );
			for (int j = 0; j < n; j++ ) {
				if ( (i!=j) && ( arr[j].length() == len  ) )  {
					
					if (sotSet.equals(  sortString( arr[j] )) ) {
						System.out.println( arr[i]+ " is anagram of "+arr[j]  );
					}
				}
			}
		}
	}

	public static void main ( String [] args ) {
		String [] tmp = { "star", "rats", "car", "arc", "arts", "stars" };
		anCheck ( tmp );
	}
	
	static String sortString ( String inputString  ) {
		char tempArray[] = inputString.toCharArray(); 
        
        // sort tempArray 
        Arrays.sort(tempArray); 
          
        // return new sorted string 
        return new String(tempArray); 
		
	}
}

package stringAlgorithm;

import java.util.ArrayList;
import java.util.List;
/* Completed. Checks if a string exists in elements of an Array
 *  by character matching
 */
public class DictArray {

	static boolean wordBreak ( String s, List<String> wordDict ) {
		
		boolean doesExist = false;
		int begin = 0;
		int end = 1;
		String tmp;
		
		for ( int i=0; i < s.length(); i++ ) {
			tmp = s.substring(begin, end );
			
			for (int j = 0; j < wordDict.size(); j++ ) {
				if ( tmp.equals( wordDict.get(j) )   ) {
					System.out.println( tmp + " Matches "+wordDict.get(j) );
					begin = end;
				}
			}
			end++;
			System.out.println( "THis is tmp "+ tmp );
		}
		
		System.out.println( "Completed" );
		return doesExist;
	}
	
	public static void main ( String args[] ) {
		
		List<String>  wordDict = new ArrayList<String>();
		wordDict.add("apple");
		wordDict.add("ppen");
		
		wordBreak( "pen", wordDict ); 
		
	}
}

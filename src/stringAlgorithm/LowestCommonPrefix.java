package stringAlgorithm;

public class LowestCommonPrefix {
	 /* (1)  Longest Common Prefix Word by word matching &
	  * (2)  Longest Common Prefix Character by Character matching
	  */
	
	/*
	 *  (1) Longest Common Prefix Word by word matching
	 */
	 static String commonPrefixUtil (String str1, String str2 ) {
		 
		 String result = "";
		 int n1 = str1.length(), n2 = str2.length();
		 
		 //System.out.println(  "Str1 = "+str1 +", Str2 = "+str2 );
		 
		 for ( int i = 0, j = 0; i <= (n1 - 1) && j <= n2 - 1; i++, j++ ) {
			 if ( str1.charAt(i) != str2.charAt(j) )  {
				 break;
			 }
			 result += str1.charAt(i);
		 }
		 System.out.println( "result = "+result );
		 return (result);
	 }
	 
	
	 static String commonPrefix ( String[] arr, int n ) {
		 String prefix = arr[0];
		 
		 for (int i = 1; i< arr.length; i++ ) {
			 prefix = commonPrefixUtil(prefix, arr[i]);
			 //System.out.println( "Prefix = "+prefix + " - arr[i] = "+ arr[i]  );
		 }		 
		 return prefix;
	 }
	
	
	 public static void main(String[] args) { 
	        String arr[] = {"geeksforgeeks", "geeks", "geek", "geezer","geeg" }; 
	        int n = arr.length; 
	  
	       // String ans = checkCharacters(arr, n); 
	        
	        String ans = checkCharacters(arr ); 
	  
	        if (ans.length() > 0) { 
	            System.out.printf("The longest common prefix is - %s", 
	                    ans); 
	        } else { 
	            System.out.printf("There is no common prefix"); 
	        } 
	    } 
	 
	 /*
	  * Longest Common Prefix Character by Character matching
	  */	 
	 static String  checkCharacters ( String[] strings ) {
		 
		 if (strings.length == 0) {
		        return "";   // Or maybe return null?
		    }

		    for (int prefixLen = 0; prefixLen < strings[0].length(); prefixLen++) {
		        char c = strings[0].charAt(prefixLen);
		        for (int i = 1; i < strings.length; i++) {
		            if ( prefixLen >= strings[i].length() ||
		                 strings[i].charAt(prefixLen) != c ) {
		                // Mismatch found
		                return strings[i].substring(0, prefixLen);
		            }
		        }
		    }
		    return strings[0];
	 }

}

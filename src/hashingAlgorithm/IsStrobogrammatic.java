package hashingAlgorithm;

import java.util.HashMap;
/* COMPLETED PROGRAM WORKS WELL 
 */

public class IsStrobogrammatic {
	
	static boolean isStrobo ( String str ) {
		HashMap<Character, Character>  hmap = new HashMap<Character, Character>();
		hmap.put( '1', '1');
		hmap.put( '0', '0');
		hmap.put( '8', '8');
		hmap.put( '6', '9');
		hmap.put( '9', '6');
		
		int left = 0, right = str.length() - 1;
		
		while ( left <= right ) {
			if ( !hmap.containsKey(str.charAt(left)) || 
					!hmap.get( str.charAt(left)).equals( str.charAt(right)) ) {
				 return false;
			}
			
			left++;
			right--;
		}		
		return true;
	}
	
	public static void main ( String[] args ) {
		boolean result  = isStrobo ( "818");
		System.out.println( "Result = "+ result  );
	}

}

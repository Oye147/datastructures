package gcdGCDAlgorithm;

public class GCDClass {
/*	(Greatest Common Divisor) or (Highest Common Factor)
 *  Using Euclidean Algorithm
 */
	public static void main ( String [] args ) {
		
		 int a = 75, b = 125; 
	      
		 // Using GCD Algorithm 
		 //System.out.println("GCD of " + a +" and " + b + " is " + gcd(a, b)); 
		 
		 
		 // Using gcdEuclid Algorithm
		 System.out.println("GCD of " + a +" and " + b + " is " + gcdEuclid(a, b)); 
	  
	}
	static int cnt = 0;
	
	@SuppressWarnings("unused")
	private static int gcd ( int a, int b) {
		/* GCD Algorithm */
		cnt++;
		System.out.println ( "Counter = "+cnt);
		// Validation case
		if ( a==0 )  return b;
		if ( b==0 )  return a;
		
		// Base Case
		if ( a==b )  return a;
		
		if ( b > a ) return gcd ( b-a, a);
		
		return gcd( b, a-b);
	}
	
	
	private static int gcdEuclid (  int a, int b ) {
		/* Extended Euclidean Algorithm */
		cnt++;
		System.out.println ( "Counter = "+cnt);
		if (a==0) return b;
		
		return gcdEuclid( b%a, a);
	}
	
	
}


